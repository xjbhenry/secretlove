package me.henryxu.weneed.util;

public interface Constant {
    // Key: pass to JobSearchFragment, and save SharePreference
    String SEARCH_KEYWORD = "keyword";
    String SEARCH_LOCATION = "location";
    String JOB_ID = "jobid";
    String PROFILE_ID = "profile_id";
}
