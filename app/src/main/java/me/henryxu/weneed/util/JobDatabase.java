package me.henryxu.weneed.util;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import me.henryxu.weneed.data.FavoriteJobDao;
import me.henryxu.weneed.model.FavoriteJob;

@Database(entities = {FavoriteJob.class}, version = 1)
public abstract class JobDatabase extends RoomDatabase {
    public abstract FavoriteJobDao favoriteJobDao();
    private static volatile JobDatabase instance;

    public static JobDatabase getDatabase(final Context context) {
        if (instance == null) {
            synchronized (JobDatabase.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            JobDatabase.class, "job_database")
                            .build();
                }
            }
        }
        return instance;
    }

    public static void destroyInstance() {
        instance = null;
    }
}
