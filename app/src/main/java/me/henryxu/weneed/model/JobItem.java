package me.henryxu.weneed.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobItem implements Serializable{
    @SerializedName("id")
    private String id;
    @SerializedName("created_at")
    private String date;
    @SerializedName("title")
    private String title;
    @SerializedName("location")
    private String location;
    @SerializedName("type")
    private String type;
    @SerializedName("description")
    private String description;
    @SerializedName("how_to_apply")
    private String applyURL;
    @SerializedName("company")
    private String company;
    @SerializedName("company_url")
    private String companyURL;
    @SerializedName("company_logo")
    private String companyLogoURL;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getApplyURL() {
        return applyURL;
    }

    public void setApplyURL(String applyURL) {
        this.applyURL = applyURL;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyURL() {
        return companyURL;
    }

    public void setCompanyURL(String companyURL) {
        this.companyURL = companyURL;
    }

    public String getCompanyLogoURL() {
        return companyLogoURL;
    }

    public void setCompanyLogoURL(String companyLogoURL) {
        this.companyLogoURL = companyLogoURL;
    }

    @Override
    public String toString() {
        return id + ',' + company;
    }
}
