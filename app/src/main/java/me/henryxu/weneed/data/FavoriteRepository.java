package me.henryxu.weneed.data;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

import me.henryxu.weneed.model.FavoriteJob;
import me.henryxu.weneed.util.JobDatabase;

public class FavoriteRepository {
    private static final FavoriteRepository ourInstance = new FavoriteRepository();

    private FavoriteJobDao favoriteJobDao;
    private JobDatabase db;
    public static FavoriteRepository getInstance() {
        return ourInstance;
    }

    private FavoriteRepository() {
    }

    public void initialDB(Application application) {
        if (db == null) {
            db = JobDatabase.getDatabase(application);
        }
        if (favoriteJobDao == null) {
            favoriteJobDao = db.favoriteJobDao();
        }
    }


    public void insert(FavoriteJob favoriteJob) {
        new insertAsyncTask(favoriteJobDao).execute(favoriteJob);
    }

    public LiveData<List<FavoriteJob>> getAllFavoriteJob() {
        return favoriteJobDao.load();
    }

    public void delete(String jobId) {
        new deleteAsyncTask(favoriteJobDao).execute(jobId);
    }

    public LiveData<FavoriteJob> isFavorite(String jobId) {
        return favoriteJobDao.getFavoriteJob(jobId);
    }

    private static class insertAsyncTask extends AsyncTask<FavoriteJob, Void, Void> {

        private FavoriteJobDao mAsyncTaskDao;

        insertAsyncTask(FavoriteJobDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final FavoriteJob... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<String, Void, Void> {

        private FavoriteJobDao mAsyncTaskDao;

        deleteAsyncTask(FavoriteJobDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final String... params) {
            mAsyncTaskDao.delete(params[0]);
            return null;
        }
    }

}
