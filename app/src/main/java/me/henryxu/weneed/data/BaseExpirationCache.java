package me.henryxu.weneed.data;

import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LruCache;

import java.util.HashMap;
import java.util.Map;

class BaseExpirationCache<K, V> {
    private LruCache<K, V> mLruCache;
    private Map<K, Long> mTimestampMap;
    private int mExpirationTime;

    BaseExpirationCache(int maxSize, int expirationTime) {
        mLruCache = new LruCache<>(maxSize);
        mTimestampMap = new HashMap<>();
        mExpirationTime = expirationTime;
    }

    boolean isValid(K k) {
        return (mTimestampMap.containsKey(k) &&
                System.currentTimeMillis() / 1000 -  mTimestampMap.get(k) <= mExpirationTime);
    }

    @Nullable
    V get(K k) {
        if(isValid(k)) {
            Log.i("bei", "hit");
            return  mLruCache.get(k);
        }
        mLruCache.remove(k);
        mTimestampMap.remove(k);
        return null;
    }

    V put(K k, V v) {
        V prev = mLruCache.put(k, v);
        mTimestampMap.put(k, System.currentTimeMillis() / 1000);
        return prev;
    }
}
