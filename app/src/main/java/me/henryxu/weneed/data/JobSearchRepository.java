package me.henryxu.weneed.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import me.henryxu.weneed.model.JobItem;
import me.henryxu.weneed.network.RetrofitClient;
import me.henryxu.weneed.util.NoConnectivityException;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobSearchRepository {
    private static final JobSearchRepository ourInstance = new JobSearchRepository();
    private JobSearchCache searchCache = new JobSearchCache(10, 20);
    private JobDetailCache jobDetailCache = new JobDetailCache(10, 20);
    private static Context mContext;

    public static JobSearchRepository getInstance(Context context) {
        mContext = context;
        return ourInstance;
    }

    private JobSearchRepository() {
    }

    public LiveData<List<JobItem>> getJobSearchList(String keyword, final String location, int page) {
        final String key = keyword + location + page;
        LiveData<List<JobItem>> cachedList = searchCache.get(key);
        if (cachedList != null) {
            Log.i("chestnut", "cache hit");
            return cachedList;
        }

        final MutableLiveData<List<JobItem>> data = new MutableLiveData<>();
        searchCache.put(key, data);
        RetrofitClient.getInstance(mContext).getJobSearchService().getJobSearchResult(keyword, location, String.valueOf(page)).enqueue(new Callback<List<JobItem>>() {
            @Override
            public void onResponse(Call<List<JobItem>> call, Response<List<JobItem>> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<JobItem>> call, Throwable t) {
                if(t instanceof NoConnectivityException) {
                    Toast.makeText(mContext, "No Connectivity", Toast.LENGTH_SHORT).show();
                    Log.e("chestnut", "Connection fail.");
                }
            }


        });
        return data;
    }

    public LiveData<JobItem> getJobItemDetail(String id) {
        LiveData<JobItem> cached = jobDetailCache.get(id);
        if (cached != null) {
            return cached;
        }
        final MutableLiveData<JobItem> data = new MutableLiveData<>();
        jobDetailCache.put(id, data);
        RetrofitClient.getInstance(mContext).getJobDetailService().getJobItemDetail(id + ".json"). enqueue(new Callback<JobItem>() {
            @Override
            public void onResponse(Call<JobItem> call, Response<JobItem> response) {
                Log.i("chestnut", String.valueOf(response.isSuccessful()));
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<JobItem> call, Throwable t) {

            }
        });
        return data;
    }
}
