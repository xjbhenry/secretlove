package me.henryxu.weneed.data;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import me.henryxu.weneed.model.FavoriteJob;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface FavoriteJobDao {
    @Insert(onConflict = REPLACE)
    void insert(FavoriteJob favoriteJob);

    @Query("DELETE FROM favorite_job")
    void deleteAll();

    @Query("DELETE FROM favorite_job WHERE id = :id")
    void delete(String id);

    @Query("SELECT * FROM favorite_job")
    LiveData<List<FavoriteJob>> load();

    @Query("SELECT * FROM favorite_job WHERE id = :id")
    LiveData<FavoriteJob> getFavoriteJob(String id);
}
