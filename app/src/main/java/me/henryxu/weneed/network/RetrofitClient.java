package me.henryxu.weneed.network;

import android.content.Context;

import me.henryxu.weneed.util.ConnectivityInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static RetrofitClient ourInstance = null;
    private JobSearchService  jobSearchService;
    private JobDetailService jobDetailService;
    private static Context mContext;

    public static RetrofitClient getInstance(Context context) {
        mContext = context;
        if(ourInstance == null) {
            ourInstance = new RetrofitClient();
        }
        return ourInstance;
    }

    private RetrofitClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new ConnectivityInterceptor(mContext))
                .build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://jobs.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        jobSearchService = retrofit.create(JobSearchService.class);
        jobDetailService = retrofit.create(JobDetailService.class);
    }

    public JobSearchService getJobSearchService() {
        return jobSearchService;
    }

    public JobDetailService getJobDetailService() {
        return jobDetailService;
    }
}
