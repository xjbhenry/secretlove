package me.henryxu.weneed.network;

import me.henryxu.weneed.model.JobItem;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface JobDetailService {
    @GET("/positions/{id}")
    Call<JobItem> getJobItemDetail(@Path("id") String id);
}
