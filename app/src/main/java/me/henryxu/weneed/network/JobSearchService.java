package me.henryxu.weneed.network;

import java.util.List;

import me.henryxu.weneed.model.JobItem;
import retrofit2.Call;
import retrofit2.http.Query;
import retrofit2.http.GET;

public interface JobSearchService {
    @GET("/positions.json")
    Call<List<JobItem>> getJobSearchResult(@Query("description") String description, @Query("location") String location, @Query("page") String page);
}
