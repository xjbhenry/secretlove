package me.henryxu.weneed.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import me.henryxu.weneed.R;
import me.henryxu.weneed.ui.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends BaseFragment {

    public static final String TAG = HomeFragment.class.getSimpleName();
    private EditText mEditTextKeyword;
    private EditText mEditTextLocation;
    private Button mButtonSearch;
    private HomeFragmentListener mListener;
    private View.OnClickListener mJobSearchBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //navigate to JobSearchFragment
            mListener.onContinueToJobSearchFragment(mEditTextKeyword.getText().toString(),
                    mEditTextLocation.getText().toString());
        }
    };

    public HomeFragment() {
        // empty public constructor
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof HomeFragmentListener) {
            mListener = (HomeFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mEditTextKeyword = rootView.findViewById(R.id.ed_home_keyword);
        mEditTextLocation = rootView.findViewById(R.id.ed_home_location);
        mButtonSearch = rootView.findViewById(R.id.btn_home_job_search);
        mButtonSearch.setOnClickListener(mJobSearchBtnListener);
        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface HomeFragmentListener {
        void onContinueToJobSearchFragment(String keyWord, String location);
    }
}
