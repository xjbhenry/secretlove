package me.henryxu.weneed.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import me.henryxu.weneed.R;
import me.henryxu.weneed.ui.fragment.FavoriteFragment;
import me.henryxu.weneed.ui.fragment.HomeFragment;
import me.henryxu.weneed.ui.fragment.JobDetailFragment;
import me.henryxu.weneed.ui.fragment.JobSearchFragment;
import me.henryxu.weneed.ui.fragment.ProfileFragment;

public class MainActivity extends AppCompatActivity
    implements HomeFragment.HomeFragmentListener, JobSearchFragment.JobSearchListener,
FavoriteFragment.FavoriteListener{

    private FragmentManager mFragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        getSupportActionBar().hide();

        mFragmentManager = getSupportFragmentManager();

        if (findViewById(R.id.fragment_container) != null) {
            if (savedInstanceState != null) {
                return;
            }
            HomeFragment homeFragment = HomeFragment.newInstance();
            mFragmentManager.beginTransaction()
                            .add(R.id.fragment_container, homeFragment)
                            .commit();
        }

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    HomeFragment homeFragment = HomeFragment.newInstance();
                    mFragmentManager.beginTransaction()
                                    .replace(R.id.fragment_container, homeFragment)
                                    .addToBackStack(homeFragment.TAG)
                                    .commit();
                    return true;
                case R.id.navigation_dashboard:
                    FavoriteFragment favoriteFragment = FavoriteFragment.newInstance();
                    mFragmentManager.beginTransaction()
                                    .replace(R.id.fragment_container, favoriteFragment)
                                    .commit();
                    return true;
                case R.id.navigation_profile:
                    ProfileFragment profileFragment = ProfileFragment.newInstance("1");
                    mFragmentManager.beginTransaction()
                                    .replace(R.id.fragment_container, profileFragment)
                                    .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onContinueToJobSearchFragment(String keyWord, String location) {
        JobSearchFragment jobSearchFragment = JobSearchFragment.newInstance(keyWord, location);
        mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, jobSearchFragment)
                        .addToBackStack(jobSearchFragment.TAG)
                        .commit();
    }

    @Override
    public void onContinueToSearchDetail(String jobId) {
        JobDetailFragment detailFragment = JobDetailFragment.newInstance(jobId);
        mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, detailFragment)
                        .addToBackStack(JobDetailFragment.TAG)
                        .commit();
    }

    @Override
    public void onContinueToFavoriteJobDetail(String jobId) {
        JobDetailFragment detailFragment = JobDetailFragment.newInstance(jobId);
        mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, detailFragment)
                        .addToBackStack(JobDetailFragment.TAG)
                        .commit();
    }
}
