package me.henryxu.weneed.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import me.henryxu.weneed.R;
import me.henryxu.weneed.databinding.ItemSearchJobBinding;
import me.henryxu.weneed.model.JobItem;

public class JobListRecyclerAdapter extends RecyclerView.Adapter<JobListRecyclerAdapter.JobItemViewHolder>
    implements View.OnClickListener{

    private JobItemClickListener mListener;
    private List<JobItem> mJobList;

    public JobListRecyclerAdapter(List<JobItem> mJobList) {
        this.mJobList = mJobList;
    }

    @NonNull
    @Override
    public JobItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemSearchJobBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_search_job, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new JobItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull JobItemViewHolder holder, int position) {
        JobItem jobItem = mJobList.get(position);
        holder.mBinding.setJobItem(jobItem);
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mJobList.size();
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onSelectJobItem((int)v.getTag());
        }
    }

    public void setOnItemClickListener(JobItemClickListener listener) {
        this.mListener = listener;
    }

    public interface JobItemClickListener {
        void onSelectJobItem(int position);
    }

    public static class JobItemViewHolder extends RecyclerView.ViewHolder {

        public final ItemSearchJobBinding mBinding;

        public JobItemViewHolder(ItemSearchJobBinding itemSearchJobBinding) {
            super(itemSearchJobBinding.getRoot());
            this.mBinding = itemSearchJobBinding;
        }
    }
}
