package me.henryxu.weneed.ui.fragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.henryxu.weneed.R;
import me.henryxu.weneed.model.FavoriteJob;
import me.henryxu.weneed.ui.BaseFragment;
import me.henryxu.weneed.ui.FavoriteJobAdapter;
import me.henryxu.weneed.viewmodel.FavoriteViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private FavoriteJobAdapter adapter;
    private List<FavoriteJob> favoriteJobList = new ArrayList<>();
    private FavoriteViewModel viewModel;
    private FavoriteListener favoriteListener;
    private FavoriteJobAdapter.FavoriteJobClickListener favoriteJobListener = new FavoriteJobAdapter.FavoriteJobClickListener() {
        @Override
        public void onSelectFavoriteItem(int position) {
            favoriteListener.onContinueToFavoriteJobDetail(favoriteJobList.get(position).getId());
        }
    };

    public FavoriteFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FavoriteFragment newInstance() {
        FavoriteFragment fragment = new FavoriteFragment();
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FavoriteListener) {
            favoriteListener = (FavoriteListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement JobSearchListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_favorite, container, false);
        recyclerView = rootView.findViewById(R.id.recycler_favorite);
        adapter = new FavoriteJobAdapter(favoriteJobList);
        adapter.setFavoriteJobClickListener(favoriteJobListener);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(FavoriteViewModel.class);
        viewModel.getAllWords().observe(this, new Observer<List<FavoriteJob>>() {
            @Override
            public void onChanged(@Nullable List<FavoriteJob> favoriteJobs) {
                favoriteJobList.clear();
                favoriteJobList.addAll(favoriteJobs);
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        favoriteListener = null;
    }

    public interface FavoriteListener {
        void onContinueToFavoriteJobDetail(String jobId);
    }
}
