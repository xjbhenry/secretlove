package me.henryxu.weneed.ui.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import me.henryxu.weneed.R;
import me.henryxu.weneed.databinding.FragmentProfileBinding;
import me.henryxu.weneed.model.Profile;
import me.henryxu.weneed.ui.BaseFragment;

import static me.henryxu.weneed.util.Constant.PROFILE_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends BaseFragment {

    private String mProfileId;
    private FragmentProfileBinding mBinding;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param profileId Profile ID.
     *
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String profileId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(PROFILE_ID, profileId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProfileId = getArguments().getString(PROFILE_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_profile, container, false);
        mBinding.setProfile(getProfile());
        return mBinding.getRoot();
    }

    private Profile getProfile() {
        // suppose to use id to get profile from network calling
        // mock read from local json
        Profile profile = new Profile();
        profile.setFirstname("Jiabei");
        profile.setLastname("Xu");
        profile.setCurrentCompany("Indeed");
        profile.setCurrentPosition("Software Engineer - Android");
        profile.setEmail("goodjob@gmail.com");
        profile.setLocation("San Jose");
        profile.setMobile("(888)-888-8888");
        profile.setExperience("3+");
        return profile;
    }

}
