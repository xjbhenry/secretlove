package me.henryxu.weneed.ui.fragment;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import me.henryxu.weneed.R;
import me.henryxu.weneed.databinding.FragmentJobDetailBinding;
import me.henryxu.weneed.model.FavoriteJob;
import me.henryxu.weneed.model.JobItem;
import me.henryxu.weneed.ui.BaseFragment;
import me.henryxu.weneed.viewmodel.JobDetailViewModel;

import static me.henryxu.weneed.util.Constant.JOB_ID;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link JobDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobDetailFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String TAG = JobDetailFragment.class.getSimpleName();
    // TODO: Rename and change types of parameters
    private String mJobId;
    private JobDetailViewModel viewModel;
    private Button mBtnDetailApply;
    private FragmentJobDetailBinding mBinding;

    private View.OnClickListener mJobApplyListener= new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //Toast
        }
    };

    private View.OnClickListener addFavoriteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            viewModel.addFavorite();
        }
    };

    private View.OnClickListener removeFavoriteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            viewModel.removeFavorite();
        }
    };


    public JobDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param jobId
     *
     * @return A new instance of fragment JobDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobDetailFragment newInstance(String jobId) {
        JobDetailFragment fragment = new JobDetailFragment();
        Bundle args = new Bundle();
        args.putString(JOB_ID, jobId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mJobId = getArguments().getString(JOB_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_job_detail, container, false);
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.fragment_job_detail, container, false);
        mBtnDetailApply = rootView.findViewById(R.id.btn_detail_apply);
        mBtnDetailApply.setOnClickListener(mJobApplyListener);
        mBinding.btnLikeJob.setOnClickListener(addFavoriteListener);
        mBinding.btnUnlikeJob.setOnClickListener(removeFavoriteListener);
        return mBinding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(JobDetailViewModel.class);
        viewModel.init(mJobId);
        viewModel.getJobItem().observe(this, new Observer<JobItem>() {
            @Override
            public void onChanged(@Nullable JobItem item) {
                mBinding.setJobDetail(viewModel);
                mBinding.tvDetailDescription.setText(Html.fromHtml(item.getDescription()));
            }
        });
        viewModel.getFavorite(mJobId).observe(this, new Observer<FavoriteJob>() {
            @Override
            public void onChanged(@Nullable FavoriteJob favoriteJob) {
                mBinding.btnUnlikeJob.setVisibility(favoriteJob != null ? View.VISIBLE : View.GONE);
                mBinding.btnLikeJob.setVisibility(favoriteJob == null ? View.VISIBLE : View.GONE);
            }
        });
    }
}
