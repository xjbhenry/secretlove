package me.henryxu.weneed.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.henryxu.weneed.R;
import me.henryxu.weneed.model.JobItem;
import me.henryxu.weneed.ui.BaseFragment;
import me.henryxu.weneed.ui.JobListRecyclerAdapter;
import me.henryxu.weneed.viewmodel.JobSearchViewModel;

import static me.henryxu.weneed.util.Constant.*;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link JobSearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class JobSearchFragment extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    public static final String TAG = JobSearchFragment.class.getSimpleName();
    private EditText mEditTextKeyword;
    private EditText mEditTextLocation;
    private TextView mTextPrevPage;
    private TextView mTextNextPage;
    private ImageButton mButtonSearch;
    private RecyclerView mRecyclerSearch;
    private List<JobItem> displayList = new ArrayList<>();
    private JobListRecyclerAdapter adapter;
    private String mKeyWord;
    private String mLocation;
    private JobSearchViewModel viewModel;
    private View.OnClickListener mJobSearchBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mKeyWord = mEditTextKeyword.getText().toString();
            mLocation = mEditTextLocation.getText().toString();
            viewModel.init(mKeyWord, mLocation);
            viewModel.getJobItemList().observe(getActivity(), new Observer<List<JobItem>>() {
                @Override
                public void onChanged(@Nullable List<JobItem> jobItems) {
                    Log.v("chestnut", "" +  jobItems.size());
                    displayList.clear();
                    displayList.addAll(jobItems);
                    adapter.notifyDataSetChanged();
                    mTextPrevPage.setVisibility(viewModel.haxPrev() ? View.VISIBLE : View.INVISIBLE);
                    mTextNextPage.setVisibility(viewModel.hasNext() ? View.VISIBLE : View.INVISIBLE);

                }
            });
        }
    };

    private JobSearchListener mListener;

    public JobSearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param keyword Parameter 1.
     * @param location Parameter 2.
     *
     * @return A new instance of fragment JobSearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static JobSearchFragment newInstance(String keyword, String location) {
        JobSearchFragment fragment = new JobSearchFragment();
        Bundle args = new Bundle();
        args.putString(SEARCH_KEYWORD, keyword);
        args.putString(SEARCH_LOCATION, location);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof JobSearchListener) {
            mListener = (JobSearchListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement JobSearchListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mKeyWord = getArguments().getString(SEARCH_KEYWORD);
            mLocation = getArguments().getString(SEARCH_LOCATION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_job_search, container, false);
        mEditTextKeyword = rootView.findViewById(R.id.ed_search_keyword);
        mEditTextKeyword.setText(mKeyWord);
        mEditTextLocation = rootView.findViewById(R.id.ed_search_location);
        mEditTextLocation.setText(mLocation);
        mTextPrevPage = rootView.findViewById(R.id.btn_search_prev);
        mTextPrevPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("chestnut","decrease");

                viewModel.decreasePage();
                viewModel.getJobItemList().observe(getActivity(), new Observer<List<JobItem>>() {
                    @Override
                    public void onChanged(@Nullable List<JobItem> jobItems) {
                        displayList.clear();
                        displayList.addAll(jobItems);
                        adapter.notifyDataSetChanged();
                        mTextPrevPage.setVisibility(viewModel.haxPrev() ? View.VISIBLE : View.INVISIBLE);
                        mTextNextPage.setVisibility(viewModel.hasNext() ? View.VISIBLE : View.INVISIBLE);
                    }
                });
            }
        });
        mTextNextPage = rootView.findViewById(R.id.btn_search_next);
        mTextNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("chestnut","increase");
                viewModel.increasePage();
                viewModel.getJobItemList().observe(getActivity(), new Observer<List<JobItem>>() {
                    @Override
                    public void onChanged(@Nullable List<JobItem> jobItems) {
                        displayList.clear();
                        displayList.addAll(jobItems);
                        adapter.notifyDataSetChanged();
                        mTextPrevPage.setVisibility(viewModel.haxPrev() ? View.VISIBLE : View.INVISIBLE);
                        mTextNextPage.setVisibility(viewModel.hasNext() ? View.VISIBLE : View.INVISIBLE);
                    }
                });
            }
        });
        mButtonSearch = rootView.findViewById(R.id.btn_search_job_search);
        mButtonSearch.setOnClickListener(mJobSearchBtnListener);
        mRecyclerSearch = rootView.findViewById(R.id.recycler_search);
        adapter = new JobListRecyclerAdapter(displayList);
        adapter.setOnItemClickListener(new JobListRecyclerAdapter.JobItemClickListener() {
            @Override
            public void onSelectJobItem(int position) {
                mListener.onContinueToSearchDetail(displayList.get(position).getId());
            }
        });
        mRecyclerSearch.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerSearch.setAdapter(adapter);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(JobSearchViewModel.class);
        viewModel.init(mKeyWord, mLocation);
        viewModel.getJobItemList().observe(this, new Observer<List<JobItem>>() {
            @Override
            public void onChanged(@Nullable List<JobItem> jobItems) {
                displayList.clear();
                displayList.addAll(jobItems);
                adapter.notifyDataSetChanged();
                mTextPrevPage.setVisibility(viewModel.haxPrev() ? View.VISIBLE : View.INVISIBLE);
                mTextNextPage.setVisibility(viewModel.hasNext() ? View.VISIBLE : View.INVISIBLE);

            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface JobSearchListener {
        void onContinueToSearchDetail(String jobId);
    }

}
