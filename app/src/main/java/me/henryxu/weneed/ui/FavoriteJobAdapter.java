package me.henryxu.weneed.ui;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import me.henryxu.weneed.R;
import me.henryxu.weneed.databinding.ItemFavoriteJobBinding;
import me.henryxu.weneed.model.FavoriteJob;

public class FavoriteJobAdapter extends RecyclerView.Adapter<FavoriteJobAdapter.FavoriteItemViewHolder>implements View.OnClickListener{

    public interface FavoriteJobClickListener {
        void onSelectFavoriteItem(int position);
    }

    private List<FavoriteJob> mJobList;
    private FavoriteJobClickListener mListener;

    public FavoriteJobAdapter(List<FavoriteJob> mJobList) {
        this.mJobList = mJobList;
    }

    @NonNull
    @Override
    public FavoriteItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemFavoriteJobBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_favorite_job, parent, false);
        binding.getRoot().setOnClickListener(this);
        return new FavoriteItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteItemViewHolder holder, int position) {
        FavoriteJob favoriteJob = mJobList.get(position);
        holder.mBinding.setFavoriteItem(favoriteJob);
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return mJobList.size();
    }

    @Override
    public void onClick(View v) {
        if (mListener != null) {
            mListener.onSelectFavoriteItem((int)v.getTag());
        }
    }

    public void setFavoriteJobClickListener(FavoriteJobClickListener favoriteJobListener) {
        this.mListener = favoriteJobListener;
    }

    public static class FavoriteItemViewHolder extends RecyclerView.ViewHolder {
        public final ItemFavoriteJobBinding mBinding;

        public FavoriteItemViewHolder(ItemFavoriteJobBinding itemFavoriteJobBinding) {
            super(itemFavoriteJobBinding.getRoot());
            this.mBinding = itemFavoriteJobBinding;
        }
    }
}
