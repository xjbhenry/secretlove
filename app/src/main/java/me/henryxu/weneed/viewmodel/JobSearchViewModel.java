package me.henryxu.weneed.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.util.Log;

import java.util.List;

import me.henryxu.weneed.data.JobSearchRepository;
import me.henryxu.weneed.model.JobItem;

public class JobSearchViewModel extends AndroidViewModel {
    private String searchKey;
    private String searchLocation;
    private LiveData<List<JobItem>> jobItemList;
    private JobSearchRepository repository;
    private int page = 0;

    public JobSearchViewModel(Application application) {
        super(application);
        this.repository = JobSearchRepository.getInstance(application);
    }

    public void init(String searchKey, String searchLocation) {
        page = 0;
        this.searchKey = searchKey;
        this.searchLocation = searchLocation;
        updateJobList();
    }

    public LiveData<List<JobItem>> getJobItemList() {
        return jobItemList;
    }

    public void increasePage() {
        page++;
        updateJobList();
    }

    public boolean hasNext() {
        return jobItemList.getValue().size() == 50;
    }

    public void decreasePage() {
        page--;
        updateJobList();
    }

    public boolean haxPrev() {
        return page > 0;
    }

    private void updateJobList() {
        jobItemList = repository.getJobSearchList(searchKey, searchLocation, page);
    }
}
