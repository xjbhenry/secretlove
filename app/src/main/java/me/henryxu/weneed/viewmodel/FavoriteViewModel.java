package me.henryxu.weneed.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

import me.henryxu.weneed.model.FavoriteJob;
import me.henryxu.weneed.data.FavoriteRepository;

public class FavoriteViewModel extends AndroidViewModel {

    private FavoriteRepository repository;

    public FavoriteViewModel (Application application) {
        super(application);
        repository = FavoriteRepository.getInstance();
        repository.initialDB(application);
    }

    public LiveData<List<FavoriteJob>> getAllWords() { return repository.getAllFavoriteJob(); }

    public void insert(FavoriteJob favoriteJob) { repository.insert(favoriteJob); }

    public void delete(String jobId){
        repository.delete(jobId);
    }

}
