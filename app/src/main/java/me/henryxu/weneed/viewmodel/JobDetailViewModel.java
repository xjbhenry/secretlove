package me.henryxu.weneed.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import me.henryxu.weneed.model.FavoriteJob;
import me.henryxu.weneed.data.FavoriteRepository;
import me.henryxu.weneed.data.JobSearchRepository;
import me.henryxu.weneed.model.JobItem;

public class JobDetailViewModel extends AndroidViewModel {
    private LiveData<JobItem> jobItem;
    private JobSearchRepository jobSearchRepository;
    private FavoriteRepository favoriteRepository;

    public JobDetailViewModel(Application application) {
        super(application);
        this.jobSearchRepository = JobSearchRepository.getInstance(application);
        this.favoriteRepository = FavoriteRepository.getInstance();
        this.favoriteRepository.initialDB(application);
    }

    public void init(String id) {
        jobItem = jobSearchRepository.getJobItemDetail(id);
    }

    public LiveData<JobItem> getJobItem() {
        return jobItem;
    }

    public void addFavorite() {
        favoriteRepository.insert(new FavoriteJob(jobItem.getValue().getId(),
                jobItem.getValue().getTitle(),
                jobItem.getValue().getCompany(),
                jobItem.getValue().getLocation()));
    }

    public void removeFavorite() {
        favoriteRepository.delete(jobItem.getValue().getId());
    }

    public LiveData<FavoriteJob> getFavorite(String jobId) {
        return favoriteRepository.isFavorite(jobId);
    }
}
